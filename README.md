# Sony Xperia XA1 Plus - LineageOS 15.0 (WIP)
==============

# About Device

![Xperia XA1 Plus](res/sony_xa1_plus.jpg)

### Specifications

Component Type | Details
-------:|:-------------------------
CPU     | Octa-core 2.3GHz MT6757V Mediatek Helio P20
GPU     | Mali-T880 MP2
Memory  | 3/4 GB RAM
Shipped Android Version | 	Android 7.0 Nougat
Storage | 32GB
Battery | 3430 mAh
Display | 5.5" 1080 x 1920 px DPI 401
Primary Camera | 23 MP LED flash, HDR, panorama. Video 1080p@30fps, HDR
Selfie Camera | 8 MP 23mm (wide), 1/4", AF. Video 1080p@30fps

---
